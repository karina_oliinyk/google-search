# GOODLE-SEARCH

Simplified Google start page layout.
* using React library

### Clone the project and don't forget to instal npm:

```
git clone git@gitlab.com:karina_oliinyk/google-search.git
cd google-search
npm i
```

### Run the project

```
npm start
```
